# main.py for raytracer2014
# Geoffrey Matthews
import os

import pygame
from pygame.locals import *

from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *
from randSphere import *

if __name__ == "__main__":
    main_dir = os.getcwd()
else:
    main_dir = os.path.split(os.path.abspath(__file__))[0]
data_dir = os.path.join(main_dir, 'data')


def handleInput(screen):
    for event in pygame.event.get():
        if event.type == QUIT:
            return True
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                return True
            elif event.key == K_s:
                pygame.event.set_blocked(KEYDOWN | KEYUP)
                fname = raw_input("File name? ")
                pygame.event.set_blocked(0)
                pygame.image.save(screen, fname)
    return False


def main():
    pygame.init()
    screen = pygame.display.set_mode((512, 512))
    pygame.display.set_caption('Raytracing!')

    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((250, 0, 0))

    # clock = pygame.time.Clock()
    # clock.tick(2)

    screen.blit(background, (0, 0))
    pygame.display.flip()




    going = True
    pixelsize = 64  # power of 2
    width, height = screen.get_size()
    # world = World([Sphere((0, 0, 0), 3, Phong(color=(1, 0, 0))),
    #                Sphere((-2, -2, -2), 3, Reflector(color=(1, 1, 1)), False),
    #                Sphere((3, 3, 3), 3, Phong(color=(0, 0, 1))),
    #                Plane((0, -10, -100), (0, 1, 1), Flat(color=(0, .5, 0)))],
    #               [Light((5, 4, 5), (0, 0, 0))],
    #               Camera())
    world = World([Sphere((2,2,-5),1,Phong(color=(1,0,0))),
                    Sphere((2,-2,0),1,Phong(color=(.5,.5,.25))),
                    Sphere((-2,0,-.5),1,Phong(color=(0,0,1))),
                    Sphere((-5,2,1),1,Phong(color=(0,0,1))),
                    Sphere((-4,1,0),1,Phong(color=(0,0,1))),
                    Sphere((2,8,.5),1,Reflector(color=(1,0,0))),
                    Sphere((5,4,2),2,Phong(color=(1,0,.5))),
                    cubePlaneBlob(),
                    Plane((0, 10, 0), (0, -1, 0), Image("plane02.jpg", 64), False),
                    Plane((0, -2, 0), (0, 1, 0), Image("plane01.jpg", 64), False)],
                    [Light()],
                    Camera())
    while going:
        going = not (handleInput(screen))
        while pixelsize > 0:
            for x in range(0, width, pixelsize):
                xx = x / float(width)
                for y in range(0, height, pixelsize):
                    # clock.tick(2)
                    yy = y / float(height)
                    # draw into background surface
                    color = world.colorAt(xx, yy)
                    # print ("color",  color)
                    color = [int(255 * c) for c in color]
                    r, g, b = color
                    # if color != [127,127,127]:
                    #     print(color)
                    color = pygame.Color(r, g, b, 255)  #.correct_gamma(1/2.2)
                    background.fill(color, ((x, y), (pixelsize, pixelsize)))

                    screen.blit(background, (0, 0))
                    pygame.display.flip()
                if handleInput(screen):
                    return
                    # draw background into screen
            screen.blit(background, (0, 0))

            print(pixelsize)
            pygame.display.flip()
            pixelsize /= 2

# if this file is executed, not imported
if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()
