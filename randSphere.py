import random

from shape import *
from world import *
from material import *

def randomSphere():
    x = random.random()*10 - 5
    y = random.random()*10 - 5
    z = random.random()*10 - 5
    r = random.random()
    g = random.random()
    b = random.random()
    radius = random.random()*1.5 + .5
    return Sphere((x,y,z),radius,Phong(color=(r,g,b)))

class RandomSpheres(World):
    def __init__(self,n=20,lights=None,camera=None):
        objects = [randomSphere() for i in range(n)]
        World.__init__(self,objects,lights,camera)