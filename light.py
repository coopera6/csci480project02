from vectors import *


class Light():
    def __init__(self, direction = (1,1,1), color=(1,1,1)):
        self.direction = normalize(vec(direction))
        self.color = vec(color)
