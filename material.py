import numpy
import pygame
import os

from numpy import clip
from vectors import *
from ray import Ray
from math import floor


class Material():
    def colorAt(self, point, normal, ray, world, depth):
        return vec(1, 1, 1)


class Flat(Material):
    def __init__(self, color=(0.25, 0.5, 1.0), ambient = 0.2):
        self.color = vec(color)
        self.ambient = ambient

    def colorAt(self, point, normal, ray, world, depth):
        return shadowColor(self.color, point, world)


class Phong(Material):
    def __init__(self,
                 color=(0.25, 0.5, 1.0),
                 specularColor=(1, 1, 1),
                 ambient=0.2,
                 diffuse=0.5,
                 specular=0.5,
                 shiny=16):
        self.color = vec(color)
        self.specularColor = vec(specularColor)
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny

    def colorAt(self, point, normal, ray, world, depth):
        if numpy.dot(normal, ray.vector) > 0:
            print("inside error")
        color = self.ambient * self.color

        color = shadowColor(color, point, world)
        for l in world.lights:
            reflection = normalize(reflect(-l.direction, vec(normal)))
            color += self.diffuse * max(0, numpy.dot(l.direction, normal)) * self.color
            color += self.specular * self.specularColor * max(0, numpy.dot(reflection, ray.vector))**self.shiny
        return clip(color, 0.0, 1.0)

class Reflector(Phong):
    def colorAt(self, point, normal, eyeray, world, depth):
        color = world.neutral
        if depth > 5:
            return color
        reflection = reflect(-eyeray.vector, normal)
        newray = Ray(point, reflection)
        objecthit = newray.closestHit(world)
        '''dist,point,normal,obj'''
        if objecthit is not None:
            color = objecthit[3].material.colorAt(objecthit[1], objecthit[2], newray, world, depth+1)
            color = color * .75


        for l in world.lights:
            newray = Ray(point, l.direction)
            objecthit = newray.closestHit(world)
        # #     '''dist,point,normal,obj'''
            if objecthit is not None and objecthit[3].castShadows() is True:
                return color
            reflection = normalize(reflect(-l.direction, vec(normal)))
            color += self.diffuse * max(0, numpy.dot(l.direction, normal)) * color
            color += self.specular * self.specularColor * max(0, numpy.dot(reflection, eyeray.vector))**self.shiny
        return clip(color, 0.0, 1.0)

class Image(Flat):
    def __init__(self, file, imageScale):
        try:
            image = pygame.image.load(file)
        except pygame.error, message:
            print 'Cannot load image:', file
            raise SystemExit, message

        self.image = image.convert()
        '''#of pixels per unit in worldspace'''
        self.imageScale = imageScale

    def colorAt(self, point, normal, ray, world, depth):
        x = point[0]*self.imageScale
        u = int(round(x)) % self.image.get_width()
        z = point[2]*self.imageScale
        v = int(round(z)) % self.image.get_height()
        color = self.image.get_at((u, v))
        r = float(color[0])/255
        g = float(color[1])/255
        b = float(color[2])/255
        colorf = vec((r,g,b))
        colorf = shadowColor(colorf, point, world)
        return colorf
"""finds out if there is a shadow at the given point, if so adjust color"""
def shadowColor(color, point, world):
    for l in world.lights:
        newray = Ray(point, l.direction)
        objecthit = newray.closestHit(world)
        '''dist,point,normal,obj'''
        if objecthit is not None and objecthit[3].castShadows() is True:
            return color * .02
        return  color