from vectors import *
from material import *

EPSILON = 1.0e-10


class GeometricObject():
    def hit(self, ray):
        """Returns (t, point, normal, object) if hit
        and t > EPSILON"""
        '''t is distance from original point'''
        return (None, None, None, None)

    def castShadows(self):
        return self.shadow


class Sphere(GeometricObject):
    def __init__(self, point=(0, 0, 0), radius=1,
                 material=None, shadow=True):
        if not (material):
            material = Phong()
        self.point = vec(point)
        self.radius = radius
        self.material = material
        self.shadow = shadow

    def hit(self, ray):
        '''does the ray intersect the sphere'''
        p = vec(ray.point - self.point)
        a = numpy.dot(ray.vector, ray.vector)
        b = 2 * numpy.dot(p, ray.vector)
        c = numpy.dot(p, p) - (self.radius ** 2)

        d = b * b - (4 * a * c)
        if d > 0.0:
            d = math.sqrt(d)
            t = (-b - d) / (2*a)
            if t > EPSILON:
                intersect = ray.point + (ray.vector * t)
                # print("HIT", ray.vector)
                '''at least one hit, and t will always be closer of the two hits'''
                ''' (distance, point on object, normal to sphere, the sphere) '''''
                return t, intersect, normalize(intersect - self.point), self
        return None


class Plane(GeometricObject):
    def __init__(self, point, normal, material=None, shadow=False):
        self.point = vec(point)
        self.normal = vec(normal)
        self.shadow = shadow
        if not material:
            material = Phong()
        self.material = material

    def hit(self, ray):
        t = 0
        """check if it intersects the plane, then checks if the ray hits the
        front or back of the plane"""
        vn = numpy.dot(self.normal, ray.vector)
        # print vn
        pr = vec(self.point - ray.point)

        if vn == 0:
            """ray is in the plane"""
            return None
        t = numpy.dot(self.normal, pr) / vn
        # print t
        if vn > 0:
            """same direction"""
            return [-1, -t]
        elif t >= EPSILON:
            intersect = vec(ray.point + t * ray.vector)
            return (t, intersect, self.normal, self)
            ''' (distance, point on object, normal to plane, the plane) '''
        return None

class PlaneBlob(GeometricObject):
    def __init__(self, planes, shadow=True):
        self.planes = planes
        self.shadow = shadow
        # self.material
    def hit(self, ray):
        enter = []
        exiting = []
        closest = [None, None, None, None]

        for n in self.planes:
            dist = n.hit(ray)
            if dist is not None and dist[0] == -1:
                exiting.append(-dist[1])
            elif dist is not None:
                enter.append(dist[0])
                if closest[0] > dist[0] or closest[0] is None:
                    closest = dist

        if enter and exiting:
            if max(enter) <= min(exiting):
                return closest
        return None

def cubePlaneBlob():
    planes = []
    left = Plane(point=vec((-1,0,0)), normal=vec((-1,0,0)))
    planes.append(left)
    right = Plane(point=vec((1,0,0)), normal=vec((1,0,0)))
    planes.append(right)
    up = Plane(point=vec((0,-1,0)), normal=vec((0,-1,0)))
    planes.append(up)
    down = Plane(point=vec((0,1,0)), normal=vec((0,1,0)))
    planes.append(down)
    front = Plane(point=vec((0,0,-1)), normal=vec((0,0,-1)))
    planes.append(front)
    back = Plane(point=vec((0,0,1)), normal=vec((0,0,1)))
    planes.append(back)
    return PlaneBlob(planes)