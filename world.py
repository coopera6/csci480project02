from ray import *
class AbstractWorld():
    def colorAt(self, x, y):
        pass
class World(AbstractWorld):
    def __init__(self,
                 objects=None,
                 lights=None,
                 camera=None,
                 maxDepth = 0,
                 neutral = (.5,.5,.5),
                 nsamples = 1,
                 gamma = 1):
        self.objects = objects
        self.lights = lights
        self.camera = camera
        self.maxDepth = maxDepth
        self.neutral = vec(neutral)
        self.nsamples = nsamples
        self.gamma = gamma
    def colorAt(self, x, y):
        camray = self.camera.ray(x, y)
        ''' (distance, point on object, normal to object, the object itself) '''''
        obj = camray.closestHit(self)
        '''dist,point,normal,obj'''
        if obj is not None:
            color = obj[3].material.colorAt(obj[1], obj[2], camray, self, 0)
            if color is not None:
                return numpy.clip(color, 0, 1)
        else:
            return self.neutral
