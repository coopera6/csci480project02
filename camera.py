import numpy

from ray import *


class AbstractCamera():
    def ray(self, x, y):
        return Ray((0,0,0),(0,0,-1))

class Camera(AbstractCamera):
    def __init__(self,
            eye = (0,0,10),
            ul = (-10,10,-10),
            ur = (10,10,-10),
            ll = (-10,-10, -10),
            lr = (10,-10,-10)):
        self.eye = vec(eye)
        self.ul = vec(ul)
        self.ur = vec(ur)
        self.ll = vec(ll)
        self.lr = vec(lr)

    def ray(self, x, y):
        # print(x, x*self.ul, numpy.multiply(x,self.ul))
        r1 = numpy.multiply(1-x, self.ul) + numpy.multiply(x, self.ur)
        r2 = numpy.multiply(1-x, self.ll) + numpy.multiply(x, self.lr)

        v = (1-y)*r1 + y*r2
        # print(point - self.eye)
        return Ray(self.eye, normalize(v))
    def lookAt(self, eye, focus, up, fovy, aspect):
        self.eye = eye
        fwdvec = focus - eye

        upper = fwdvec + fovy*up/2
        return None



